package com.ffdsregisterapi;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.*;

@RestController
public class Controller {


    @CrossOrigin(origins = "*")
    @PostMapping (value="/setting")
    public void result(@RequestBody String data) {

        try{
            File ff=new File("conf/setting.json");
            ff.createNewFile();
            FileWriter ffw=new FileWriter(ff);
            ffw.write(data);
            ffw.write("\n");
            ffw.close();
        } catch (Exception e) {}

    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/setting/load", produces= MediaType.APPLICATION_JSON_VALUE)
    public String setting() {
        String chaine="";
        String fichier ="conf/setting.json";
        try{
            InputStream ips=new FileInputStream(fichier);
            InputStreamReader ipsr=new InputStreamReader(ips);
            BufferedReader br=new BufferedReader(ipsr);
            String ligne;
            while ((ligne=br.readLine())!=null){
                chaine+=ligne+"\n";
            }
            br.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        return chaine;
    }


}