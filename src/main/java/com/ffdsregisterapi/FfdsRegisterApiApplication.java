package com.ffdsregisterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

public class FfdsRegisterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FfdsRegisterApiApplication.class, args);
	}

}
